<?php

/**
 * Class W_Migration_Step
 *
 */
abstract class W_Migration_Step
{
    /**
     * @throws Exception
     */
    protected function up()
    {
        throw new Exception('W_Migration_Step::up() required implementation in extend clause');
    }

    /**
     * @throws Exception
     */
    protected function down()
    {
        throw new Exception('W_Migration_Step::down() required implementation in extend clause');
    }

    /**
     * Те миграции, которые используют UPDATE/INSERT запросы напрямую в базу,
     * будут создавать рассинхрон основной базы и кеш-среза.
     * Нам необходимо, чтобы при деплое jenkins понимал, а были ли применены такие миграции,
     * которые рассинхронили базы.
     */
    protected $needRecreateCacheDb = true;

    /**
     * @return bool
     */
    public function isNeedRecreateCacheDb(): bool
    {
        return (bool)$this->needRecreateCacheDb;
    }

    /**
     * Такие миграции выполнение которых может занимать продолжительное время,
     * при этом проще скачать dump и развернуть свежий образ базы, чем ожидать завершения миграции.
     * В том числе могут не позволять ресурсы окружения разработки.
     * Поэтому, при разработке миграций, если вы считаете что данную миграцию можно пропустить (с уведомлением),
     * то выставить в TRUE
     */
    protected $canSkipInDevEnv = false;

    /**
     * @return bool
     */
    public function isCanSkipInDevEnv(): bool
    {
        return (bool)$this->canSkipInDevEnv;
    }

    /**
     * @var bool
     */
    private $_debug = false;

    /**
     * @param bool $enable
     * @return W_Migration_Step
     */
    public function setDebug(bool $enable = false): self
    {
        if ($enable) {
            $this->_debug = true;
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isDebug(): bool
    {
        return $this->_debug;
    }

    /**
     * @param string $model
     * @return W_Migration_Step
     * @throws Exception
     */
    public function setModel(string $model): self
    {
        // instance
        W_Migration::setModel($model);

        return $this;
    }

    /**
     * Для нового контроллера WmigrateController
     * @param bool $autoChangeCommit
     * @return bool
     * @throws Exception
     */
    public function migrateApply($autoChangeCommit = false): bool
    {
        $this->logInfo('Start apply ' . get_called_class());

        if ($autoChangeCommit === false || $this->getTargetCommit() == W_Git::getCurrentCommit()) {

            $this->logInfo('Triples count: ' . W_Migration::getTriplesCount());

            try {

                $this->up();
                $this->logInfo(get_called_class() . ', triples count: ' . W_Migration::getTriplesCount());

                return true;

            } catch (\Exception $e) {

                $this->logError($e->getMessage());

                return false;
            }

        } elseif (self::checkout($this->getTargetCommit())) {
            $currentCommit = W_Git::getCurrentCommit();
            $currentBranch = W_Git::getCurrentBranch();

            $command ='php -dxdebug.remote_enable=0 cli.php migrate apply ' . get_called_class();
            $this->logInfo('Executing: ' . $command);
            passthru($command);

            return self::checkout($currentBranch === 'HEAD' ? $currentCommit : $currentBranch);
        }

        return false;
    }

    /**
     * Для нового контроллера WmigrateController
     * @param bool $autoChangeCommit
     * @return bool
     * @throws Exception
     */
    public function migrateRevert($autoChangeCommit = false): bool
    {
        $this->logInfo('Start revert ' . get_called_class());

        if ($autoChangeCommit === false || $this->getTargetCommit() == W_Git::getCurrentCommit()) {

            try {

                $this->down();
                $this->logInfo(get_called_class() . ', triples count: ' . W_Migration::getTriplesCount());

                return true;

            } catch (\Exception $e) {

                $this->logError($e->getMessage());

                return false;
            }

        } elseif (self::checkout($this->getTargetCommit())) {
            $currentBranch = W_Git::getCurrentBranch();

            $command ='php -dxdebug.remote_enable=0 cli.php migrate revert ' . get_called_class();
            $this->logInfo('Executing: ' . $command);
            passthru($command);

            return self::checkout($currentBranch === 'HEAD' ? W_Git::getCurrentCommit() : $currentBranch);
        }

        return false;
    }

    /********************************
     * ERRORS
     *******************************/

    /**
     * @var array
     */
    private $errors = [];

    /**
     * @return bool
     */
    public function hasError(): bool
    {
        return count($this->errors) > 0;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param string $msg
     */
    protected function addError(string $msg)
    {
        $this->errors[] = $msg;
    }

    /********************************
     * HELPERS
     *******************************/

    /**
     * Целевой коммит. Высчитывается в методе getTargetCommit, но при необходимости
     * можно установить в своем классе полный хеш необходимого коммита
     * @var string
     */
    protected $targetCommit = false;

    /**
     * Получить хеш коммита, из под которого должна запуститься данная миграция.
     * В основном мы берем коммит, из под которого было совершено последнее изменение над миграцией.
     * Но, если мы находимся в режим разработки и миграция еще не была закомичена, то мы просто возвращаем текущий коммит
     * @return bool|string
     */
    protected function getTargetCommit()
    {
        if (!$this->targetCommit) {
            $this->targetCommit = W_Git::getLastCommitForFile(W_Migration::MIGRATION_PATH . get_class($this) . '.php');

            if (!$this->targetCommit) {
                $this->targetCommit = W_Git::getCurrentCommit();
            }
        }

        return $this->targetCommit;
    }

    /**
     * @param string $commitHash
     * @return bool
     * @throws Exception
     */
    protected function checkout(string $commitHash): bool
    {
        if (W_Git::isValidGitHash($commitHash)) {
            $this->logInfo('checkout: ' . exec('git checkout ' . $commitHash));
            return W_Git::getCurrentCommit() == $commitHash;
        }

        return false;
    }

    /**
     * @param string $msg
     * @throws Exception
     */
    protected function logInfo(string $msg)
    {
        if ($this->isDebug()) {
            W_Migration::getLogger()->info($msg);
        }
    }

    /**
     * @param string $msg
     * @throws Exception
     */
    protected function logError(string $msg)
    {
        $this->addError($msg);

        W_Migration::getLogger()->err($msg);
    }
}