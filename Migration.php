<?php

defined('_OWROOT') or define('_OWROOT', '');

/**
 * Class W_Migration
 */
class W_Migration
{
    const MIGRATION_PATH = _OWROOT . '/migrations/';

    /**
     * @var InterfaceLogger
     */
    protected static $logger;

    /**
     * @return InterfaceLogger
     * @throws \Exception
     */
    public static function getLogger(): InterfaceLogger
    {
        if (self::$logger === null) {
            self::$logger = OntoWiki::getInstance()->getCustomLogger('Migration');
            self::$logger->addWriter(new Zend_Log_Writer_Stream('php://output'));
        }

        return self::$logger;
    }

    /**
     * @param string $model
     * @throws \Exception
     */
    public static function setModel(string $model)
    {
        // instance
        OntoWiki::getInstance()->selectedModel =
            W::store()->getModelOrCreate($model, '', Erfurt_Store::MODEL_TYPE_OWL, false);
    }

    /**
     * @return int
     */
    public static function getTriplesCount(): int
    {
        return (int)W::model()->getTriplesCount();
    }
}